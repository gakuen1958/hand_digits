# -*- coding: utf-8 -*-
'''
以下をしないと
MacOSでプログラムが強制終了する
'''
import os
os.environ['KMP_DUPLICATE_LIB_OK']='TRUE'

'''
必要なモジュールを読み込む
'''
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional\
    import Convolution2D
from keras.layers.convolutional\
    import MaxPooling2D
from keras.utils import to_categorical
from keras import backend as K
import numpy
import pandas as pd
import PIL
import cv2
import glob
import numpy as np
import tkinter as tk
from tkinter import ttk
from PIL import Image,\
    ImageDraw,ImageGrab

'''
CNNのモデルを定義する
'''
def create_model():
    global input_shape

    #分類するクラス数 0～9の数字
    num_classes = 10
    #CNNを定義する
    model = Sequential()
    #畳み込み層の定義
    #activationで活性化関数を設定 ReLu関数
    model.add(Convolution2D(32,\
        kernel_size=(3,3), activation='relu',
    input_shape=input_shape))
    #畳み込み層を追加
    model.add(Convolution2D(64,\
        (3,3), activation='relu'))
    #プーリング層を設定
    model.add(MaxPooling2D(pool_size=(2,2)))
    #ドロップアウトを定義
    model.add(Dropout(0.25))
    #全結合層を定義
    model.add(Flatten())
    model.add(Dense(256,activation='relu'))
    model.add(Dropout(0.5))
    #最終層は、softmax関数で確率に変換する
    model.add(Dense(num_classes,\
        activation='softmax'))
    #CNNをコンパイルする
    model.compile(loss=\
        'categorical_crossentropy',\
        optimizer='adam',
    metrics=['accuracy'])
    return model

'''
認識処理を行う
'''
def Recognize_Digit():
    global cv
    global model

    predictions = []
    percentage = []
    filename = 'tmp_img'
    widget = cv
    x = root.winfo_rootx() + widget.winfo_x()
    y = root.winfo_rooty() + widget.winfo_y()

    #描画した内容をepsファイルに保存する
    cv.postscript(file = filename + '.eps')
    #保存したepsファイルを読み込む
    img = Image.open(filename + '.eps')
    #epsファイルをpngファイルに変換する
    img.save(filename + '.png')

    #OpenCVを使って、カラーモードで画像を読み取る
    image = cv2.imread(filename + '.png',\
        cv2.IMREAD_COLOR)
    #カラー画像をグレースケールに変換する
    gray = cv2.cvtColor(image,\
        cv2.COLOR_BGR2GRAY)
    cv2.imwrite('gray.png', gray) #grayで保存
    #グレースケールの白黒を反転させる
    #白い部分を黒にして、黒い部分を白にする
    ret,th = cv2.threshold(gray, 0, 255,
    cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    cv2.imwrite('black_white.png', th)
    #画像サイズを横28ピクセル x 縦28ピクセルに変換する
    img = cv2.resize(th, (28 ,28),\
        interpolation=cv2.INTER_AREA)
    cv2.imwrite('small.png', img)
    #28 x 28の画像を1列に変換する
    img = img.reshape(1, 28, 28, 1)

    #255で割って、ピクセル値を小さく正規化する
    img = img / 255.0
    #モデルを使って予測を行う
    pred = model.predict([img])[0]
    #予測値を取得する
    final_pred = np.argmax(pred)
    pred_per = str(int(max(pred) * 100)) + '%'
    #予測結果を画面に表示する
    show(final_pred, pred_per)
'''
Canvasをクリアする
'''
def clear_widget():
    global cv
    global cv_result
    global recog_per

    #キャンバスをクリアする
    cv.delete("all")
    #認識結果をクリアする
    cv_result.delete("all")
    recog_per.delete("all")
'''
Canvas上でドラッグした時の処理を定義する
'''
def activate_event(event):
    global cv
    global lastx
    global lasty

    cv.bind('<B1-Motion>', draw_lines)
    lastx, lasty = event.x, event.y
'''
Canvas上でマウスドラッグした時の座標情報
を取得する
'''
def draw_lines(event):
    global cv
    global lastx
    global lasty

    x, y = event.x, event.y
    cv.create_line((lastx, lasty, x, y),\
        width=16, fill='black',
    capstyle=tk.ROUND, smooth=tk.TRUE,\
        splinesteps=16)
    lastx, lasty =x, y
'''
認識結果をCanvas上に表示する
'''
def show(res, per):
    global cv
    global cv_result
    global recog_per

    cv_result.create_text(100, 100,\
    text = res, fill='white',\
    font = ('FixedSys', 100))
    recog_per.create_text(100, 20,\
    text = per, fill='white',\
    font = ('FixedSys', 20))

#入力の形を定義する
input_shape = (28,28,1)
#モデルを作る
model = create_model()
#モデルに学習時の重みをセットする
model.load_weights('weights.h5')

#UIを生成する
root = tk.Tk()
root.resizable(0,0)
root.title("手書き数字認識-体験プログラム-")

lastx, lasty = None, None

#Canvasを横640、縦480で生成する
frame1 = ttk.Frame(root)
cv = tk.Canvas(frame1, width=640,\
height=480, bg='white')

cv.grid(row=0, column=0, pady=2,\
sticky=tk.W, columnspan=2)

#Canvasとマウスドラッグ処理を紐づける
cv.bind('<Button-1>', activate_event)

#認識ボタンを作り、認識処理を紐づける
btn_save = tk.Button(frame1, text="認識",\
command=Recognize_Digit, width=10,\
height=3)

btn_save.grid(row=2, column=0, pady=1,\
padx=1)
#削除ボタンを作り、削除処理を紐づける
button_clear = tk.Button(frame1, text="削除",\
command=clear_widget, width=10,\
height=3)

button_clear.grid(row=2, column=1,\
pady=1, padx=1)

#認識結果のエリアを作る
frame2 = ttk.Frame(root)
result_title = tk.Canvas(frame2, width=200,\
height=40, bg='white')

result_title.grid(row=0, column=0,\
sticky=tk.N)

result_title.create_text(100, 20,\
text = '認識結果',\
fill='black', font = ('FixedSys', 20))

cv_result = tk.Canvas(frame2, width=200,\
height=200, bg='gray')
cv_result.grid(row=1, column=0, sticky=tk.N)

recog_per = tk.Canvas(frame2, width=200,\
height=40, bg='gray')
recog_per.grid(row=2, column=0, sticky=tk.N)

frame1.grid(row=0, column=0)
frame2.grid(row=0, column=1, sticky=tk.N)
#画面を起動する
root.mainloop()